<!DOCTYPE html>
<html>
    <head>
        <title>SanberBook</title>
        <meta charset="utf-8">

    </head>
    <body>
        <h1>Buat Akun Baru!</h1>
        <h2>Sigh Up Form</h2>
        <!-- form -->
        <form>
            <label>
                <p>First name:</p>
                <input type="text"  placeholder="Nama depan">
            </label>
            <label>
                <p>Last name:</p>
                <input type="text"  placeholder="Nama belakang">
            </label>
            <p>Gender</p>
            <label>
                <input type="radio" name="Gender"> Male <br>
                <input type="radio" name="Gender"> Female <br>
                <input type="radio" name="Gender"> Other <br>
            </label>
            <p>Nationality</p>
            <label>
                <select name="kewarganegaraan" id="negara">
                    <option value="">indonesia</option>
                    <option value="">England</option>
                    <option value="">German</option>
                    <option value="">Arabic</option>
                </select>
            </label>
            <p>Language Spoken</p>
            <label for="">
                <input type="checkbox"> Bahasa indonesia <br>
                <input type="checkbox"> english <br>
                <input type="checkbox"> Other <br>
            </label>
            <p>Bio</p>
            <label>
                <textarea cols="30" rows="10" ></textarea>
            </label>
            <br>
            <button type="submit"><a href="welcome.html">Sigh Up</a></button>
        </form>
    </body>
</html>